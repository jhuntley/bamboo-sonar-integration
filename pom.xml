<?xml version="1.0" encoding="UTF-8"?>
<!--
 ~ Licensed to Marvelution under one or more contributor license 
 ~ agreements.  See the NOTICE file distributed with this work 
 ~ for additional information regarding copyright ownership.
 ~ Marvelution licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~  http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied. See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<parent>
		<artifactId>marvelution</artifactId>
		<groupId>com.marvelution</groupId>
		<version>17</version>
	</parent>
	<groupId>com.marvelution.bamboo.plugins</groupId>
	<artifactId>bamboo-sonar-integration</artifactId>
	<version>1.5.5-SNAPSHOT</version>
	<packaging>pom</packaging>

	<modules>
		<module>bamboo-sonar-common</module>
		<module>bamboo-sonar-tasks</module>
		<module>bamboo-sonar-web</module>
		<module>bamboo-sonar</module>
	</modules>
	
	<name>Bamboo Sonar Integration</name>
	<description>Integration projects that, through several Bamboo plugins, support the integration between Atlassian Bamboo and Sonar</description>
	<url>http://apidocs.marvelution.com/bamboo-sonar-integration/${project.version}/</url>
	<inceptionYear>2011</inceptionYear>
	<organization>
		<name>Marvelution</name>
		<url>http://www.marvelution.com/</url>
	</organization>
	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:git@bitbucket.org:marvelution/bamboo-sonar-integration.git</connection>
		<developerConnection>scm:git:git@bitbucket.org:marvelution/bamboo-sonar-integration.git</developerConnection>
		<url>https://bitbucket.org/marvelution/bamboo-sonar-integration</url>
	  <tag>HEAD</tag>
  </scm>
	<issueManagement>
		<system>JIRA</system>
		<url>http://issues.marvelution.com/browse/BAMSON</url>
	</issueManagement>
	<ciManagement>
		<system>Bamboo</system>
		<url>http://builds.marvelution.com/browse/BAMSON</url>
		<notifiers>
			<notifier>
				<type>mail</type>
				<configuration>
					<address>ci@lists.marvelution.com</address>
				</configuration>
			</notifier>
		</notifiers>
	</ciManagement>
	<distributionManagement>
		<site>
			<id>marvelution.website</id>
			<url>dav:http://repository.marvelution.com/content/sites/apidocs/bamboo-sonar-integration/${project.version}/</url>
		</site>
	</distributionManagement>

	<properties>
		<project.jdk.revision>1.6</project.jdk.revision>
		<atlassian.plugin.key.base>${project.groupId}.sonar</atlassian.plugin.key.base>
		<atlassian.plugin.key.tasks>${atlassian.plugin.key.base}.tasks</atlassian.plugin.key.tasks>
		<atlassian.plugin.key.web>${atlassian.plugin.key.base}.web</atlassian.plugin.key.web>
		<stagingSiteURL>dav:http://repository.marvelution.com/content/sites/apidocs-staging/bamboo-sonar-integration/${project.version}/</stagingSiteURL>
		<atlassian.bamboo.version>4.2.1</atlassian.bamboo.version>
		<atlassian.bamboo.data.version>3.2.2</atlassian.bamboo.data.version>
		<atlassian.ao.version>0.19.11.bamboo.4</atlassian.ao.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-api</artifactId>
			<version>${atlassian.bamboo.version}</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-web</artifactId>
			<version>${atlassian.bamboo.version}</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.atlassian.bamboo</groupId>
			<artifactId>atlassian-bamboo-ui-test</artifactId>
			<version>${atlassian.bamboo.version}</version>
			<type>test-jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.powermock</groupId>
			<artifactId>powermock-api-mockito</artifactId>
			<version>1.4.10</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.powermock</groupId>
			<artifactId>powermock-module-junit4</artifactId>
			<version>1.4.10</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.8.5</version>
			<type>jar</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.8</version>
			<type>jar</type>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<!-- preparation goals need to be clean because of Maven Release plugin issue MRELEASE-285 -->
			<plugin>
				<inherited>true</inherited>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<configuration>
					<autoVersionSubmodules>true</autoVersionSubmodules>
					<preparationGoals>clean</preparationGoals>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>license-header</id>
			<build>
				<plugins>
					<plugin>
						<inherited>true</inherited>
						<groupId>com.mycila.maven-license-plugin</groupId>
						<artifactId>maven-license-plugin</artifactId>
						<configuration>
							<excludes>
								<exclude>**/panel.js</exclude>
							</excludes>
						</configuration>
					</plugin>
			</plugins>
			</build>
		</profile>
	</profiles>

	<developers>
		<developer>
			<id>markrekveld</id>
			<name>Mark Rekveld</name>
			<url>http://www.marvelution.com</url>
			<organization>Marvelution</organization>
			<email>markrekveld@marvelution.com</email>
			<organizationUrl>http://www.marvelution.com</organizationUrl>
			<timezone>+1</timezone>
			<roles>
				<role>Marvelution Member</role>
			</roles>
		</developer>
	</developers>
</project>
